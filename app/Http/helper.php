<?php

/**
 * @param string $module_name
 * @param string $type
 * @return string
 */
 if(!function_exists('buildPrefix')){
     function buildPrefix(string $module_name,string $type){
         return config($module_name.'.prefix.'.$type,config('module.prefix.'.$type));
     }
 }


if(!function_exists('module_prefix')){
    function module_prefix($module_name){
        return config($module_name.'.module_name');
    }
}


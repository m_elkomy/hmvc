<?php

namespace Customer\Database\Seeds;

use App\Models\User;
use Customer\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Customer::create([
            'name'=>'elkomy',
        ]);
    }
}

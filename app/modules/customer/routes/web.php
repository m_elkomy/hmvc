<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['admin'])->group(function(){
    Route::namespace('Customer\Http\Controllers')->prefix(module_prefix('customer'))->group(function(){

        Route::prefix(buildPrefix('customer','backend'))->namespace('Backend')->group(function(){
            Route::get('customer','CustomerController@index');
        });

        Route::prefix(buildPrefix('customer','frontend'))->namespace('FrontEnd')->group(function(){
            Route::get('customer','CustomerController@index');
        });

    });
});

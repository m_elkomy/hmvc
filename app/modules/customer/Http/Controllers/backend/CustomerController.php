<?php

namespace Customer\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use Customer\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
class CustomerController extends Controller
{
    //
    public function index(CustomerRequest $request){
        return view('Customer::backend.index',['title'=>trans('Customer::customer.dashboard')]);
    }
}

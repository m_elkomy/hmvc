<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('HR\Http\Controllers')->prefix(module_prefix('hr'))->group(function(){


    Route::prefix(buildPrefix('hr','backend'))->namespace('Backend')->group(function(){
        Route::get('hr','HRController@index');
    });

    Route::prefix(config('hr.prefix.frontend',config('module.prefix.frontend')))->namespace('FrontEnd')->group(function(){
        Route::get('hr','HRController@index');
    });


});

<?php

namespace HR\Database\Seeds;

use App\Models\User;
use HR\Models\HR;
use Illuminate\Database\Seeder;

class HRSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HR::create([
            'name'=>'hr',
        ]);
    }
}

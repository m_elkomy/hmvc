<?php

namespace Database\Seeders;

use Customer\Database\Seeds\CustomerSeeder;
use HR\Database\Seeds\HRSeeder;
use Illuminate\Database\Seeder;
use Support\Database\Seeds\SupportSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(CustomerSeeder::class);
        $this->call(HRSeeder::class);
        $this->call(SupportSeeder::class);
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('hr/backend','\HR\Http\Controllers\Backend\HRController@index');
Route::get('hr/frontend','\HR\Http\Controllers\FrontEnd\HRController@index');

Route::get('customer/backend','\Customer\Http\Controllers\Backend\CustomerController@index');
Route::get('customer/frontend','\Customer\Http\Controllers\FrontEnd\CustomerController@index');
